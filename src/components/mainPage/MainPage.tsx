import React, { useState, useEffect } from 'react'
import BooksList from '../lists/BooksList';
import AuthorsList from '../lists/AuthorsList'
import AuthorAddForm from '../addForm/AuthorAddForm';
import BookAddForm from '../addForm/BookAddForm';
import { getAllBooks,getAllAuthors } from '../../api/BookService'
import { IAuthor, IFetchBook } from '../../types/types';

const MainPage = () => {

    const [filter, setFilter] = useState<string>('all')
    const [renderBook, setRenderBook] = useState<boolean>(true)
    const [renderAuthor, setRenderAuthor] = useState<boolean>(true)
    const [bookList, setbookList] = useState<IFetchBook[]>([])
    const [authorList, setAuthorList] = useState<IAuthor[]>([])

    useEffect(() => {
        onRequest();
        // eslint-disable-next-line
    }, [renderBook, renderAuthor])

    const onRequest = () => {
        if (renderBook) {
            getAllBooks()
            .then(bookList => setbookList(bookList))
            .then(() => setRenderBook(false))
        }
        if (renderAuthor) {
            getAllAuthors()
            .then(authorList => setAuthorList(authorList))
            .then(() => setRenderAuthor(false))
        }
    }

    const filterBooks = (items: IFetchBook[], filter: string) => {
        if (filter === 'all') {
            return items
        } else {
            return items.filter((item) => item.author_id === filter)
        }
    }

    const visibleData = filterBooks(bookList, filter)

    const onFilterSelect = (filter: string) => {
        setFilter(filter);
    }

    const onAddBook = () => {
        setRenderBook(true);
    }

    const onAddAuthor = () => {
        setRenderAuthor(true);
    }
    
    return (
        <div className='main pt-3 pb-3'>
            <BooksList
            visibleData={visibleData}
            authorList={authorList}
            filter={filter}
            onFilterSelect={onFilterSelect}
            />
            <BookAddForm
            authorList={authorList}
            onAddBook={onAddBook}
            />
            <AuthorsList
            authorList={authorList}
            />
            <AuthorAddForm
            onAddAuthor={onAddAuthor}
            />
        </div>
    )
}

export default MainPage