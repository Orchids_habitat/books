import React, { useEffect, useState } from 'react';
import Input from '../../utils/Input';
import { registration } from '../../api/user';

import './authorization.css'

const Registration = () => {
    const [name, setName] = useState<string>('')
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [password_confirmation, setPassword_confirmation] = useState<string>('')
    const [formValid, setFormValid] = useState<boolean>(false)
    const [emailError, setEmailError] = useState<string>('')

    const data = {
        name, 
        email, 
        password, 
        password_confirmation, 
        setName, 
        setEmail, 
        setPassword, 
        setPassword_confirmation
    }

    useEffect(() => {
        isValidEmail(email)
        validationForm()
        // eslint-disable-next-line
    }, [name, email, password, password_confirmation])

    const validationForm = () => {
        if ((password !== password_confirmation) || (name.length < 3) || (password.length < 6) || emailError || !email) {
            setFormValid(false)
        } else setFormValid(true)
    }

    const isValidEmail = (email: string) => { 
        const re = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/
        if (re.test(String(email).toLowerCase()) || !email) {
            setEmailError('')
        } else {
            setEmailError('Невалидный email')
        }
    } 

    return (
        <div className='authorization'>
            <div className='authorization__header'>Регистрация</div>
            <Input value={name} setValue={setName} type='text' placeholder='Введите имя (не менее 3 символов)'/>
            <Input value={email} setValue={setEmail} type='text' placeholder='Введите email'/>
            {emailError && <div style={{color: 'red'}}>{emailError}</div>}
            <Input value={password} setValue={setPassword} type='password' placeholder='Введите пароль (не менее 6 символов)'/>
            {(password !== password_confirmation && password_confirmation) && <div style={{color: 'red'}}>Пароль не совпадает</div>}
            <Input value={password_confirmation} setValue={setPassword_confirmation} type='password' placeholder='Повторите пароль...'/>
            <button disabled={!formValid} className='authorization__btn' onClick={() => registration(data)}>Зарегистрироваться</button>
        </div>
    );
};

export default Registration;