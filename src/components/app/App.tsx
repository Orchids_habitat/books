import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Navbar from '../navbar/Navbar';
import Registration from '../authorization/Registration';
import Login from '../authorization/Login';
import MainPage from '../mainPage/MainPage'
import { auth } from '../../api/user';
import Spinner from '../spinner/Spinner';
import userStore from '../../store/userStore';
import { injectStores } from '@mobx-devtools/tools';
import { observer } from 'mobx-react-lite';

import './app.css';

injectStores({
  userStore
})

const App = observer(() => {

  const [loadPage, setLoadPage] = useState<boolean>(true)

  const isAuth = userStore.state.isAuth

  useEffect(() => {
      auth(isLoaded)
      // eslint-disable-next-line
  }, [])

  const isLoaded = () => {
    setLoadPage(false)
  }

  return (
    <BrowserRouter>
    <div className='App'>
      <Navbar/>
      {loadPage ? 
        <Spinner/> : 
        <div className='wrap'>
          {!isAuth &&
            <Routes>
              <Route path={'/'} element={<Navigate to='login' />}/>
              <Route path={'registration'} element={<Registration/>}/>
              <Route path={'login'} element={<Login/>}/>
            </Routes>
          }
          {isAuth && 
          <Routes>
            <Route path={'/'} element={<MainPage/>}/>
            <Route path={'login'} element={<Navigate to='/' />}/>
          </Routes>
          }
        </div>}
    </div>
    </BrowserRouter>
  );
})

export default App;