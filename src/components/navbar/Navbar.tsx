import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import userStore from '../../store/userStore';

import './navbar.css'

const Navbar: FC = () => {
    const isAuth = userStore.state.isAuth

    return (
        <div className='navbar'>
            <div className='container'>
                <div className='navbar__header'><Link to={'/'}>BOOK LIST</Link></div>
                {!isAuth && <div className='navbar__login'><Link to='/login'>Войти</Link></div>}
                {!isAuth && <div className='navbar__registration'><Link to='/registration'>Регистрация</Link></div>}
                {isAuth && <div className='navbar__login' onClick={() => userStore.logout() }><Link to='/login'>Выйти</Link></div>}
            </div>
        </div>
    );
};

export default Navbar;