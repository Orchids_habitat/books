import { makeAutoObservable } from "mobx"
import { IUser } from "../types/types"

class UserStore {
    state = {
        currentUser: {},
        isAuth: false
    }
    constructor() {
        makeAutoObservable(this)
    }
    setUser(user: IUser) {
        this.state = {
            currentUser: user,
            isAuth: true
        }
    }
    logout() {
        localStorage.removeItem('token')
        this.state = {
            currentUser: {},
            isAuth: false
        }
    }
}

export default new UserStore()