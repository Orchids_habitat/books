import axios from 'axios' 
import { IFetchBook, IAuthor, IAddAuthor, IAddBook, IFavBook } from '../types/types';

const _api = 'https://mobile.fakebook.press/api/';

const getResource = async (url: string) => {
    let res = await fetch(url);

    if (!res.ok) {
        throw new Error(`Could not fetch ${url}, status: ${res.status}`);
    }

    return await res.json();
}

export const getAllBooks = async () => {
    const res = await getResource(`${_api}books`);
    return res.data.map(_transformBook);
}

export const getAllAuthors = async () => {
    const res = await getResource(`${_api}authors`);
    return res.data.map(_transformAuthor);
}

const _transformBook = (book: IFetchBook) => {
    return {
        name: book.name,
        author_id: book.author_id,
        id: book.id
    }
}

export const getAuthor = async (id: string) => {
    const res = await getResource(`${_api}authors/${id}`);
    return res.data.name
}

const _transformAuthor = (author: IAuthor) => {
    return {
        name: author.name,
        id: author.id
    }
}

export const addAuthor = async ({name, bio, birth_date, setName, setBio, setBirth_date, onAddAuthor}: IAddAuthor) => {
    try {
        await axios.post(`${_api}authors`, {
            name: name,
            bio: bio,
            birth_date: birth_date
        }, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        setName('')
        setBio('')
        setBirth_date('')
        onAddAuthor()
    } catch (e) {
        alert('Ошибка добавления автора')
    }
}

export const addBook = async ({name, author_id, desc, publication_date, setName, setAuthor_id, setPublication_date, setDesc, onAddBook}: IAddBook) => {
    try {
        await axios.post(`${_api}books`, {
            name: name,
            author_id: author_id,
            desc: desc,
            publication_date: publication_date
        }, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        setName('')
        setAuthor_id('')
        setPublication_date('')
        setDesc('')
        onAddBook()
    } catch (e) {
        alert('Ошибка добавления книги')
    }
}

export const favBook = async ({id, action, alertMessage}: IFavBook) => {
    try {
        await axios.post(`${_api}books/${id}/${action}-favorites`, {}, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })        
        console.log(alertMessage)
    } catch (e) {
        alert('Ошибка избранного')
    }
}