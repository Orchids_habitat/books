import React, { FC } from 'react';
import BooksListItem from '../listItems/BooksListItem';
import AppFilter from '../appFilter/AppFilter';
import { IFetchBook, IAuthor } from '../../types/types';

import './lists.css';

interface BookListProps {
    visibleData: IFetchBook[], 
    authorList: IAuthor[], 
    onFilterSelect: (filter: string) => void, 
    filter: string
}

const BookList: FC<BookListProps> = ({visibleData, authorList, onFilterSelect, filter}) => {

    const itemsBooks = visibleData.map((itemBook: IFetchBook) => {
        const authorName = authorList.filter((itemAuthor: IAuthor) => (itemAuthor.id === itemBook.author_id))
        return (
            <BooksListItem key = {itemBook.id} {...itemBook} authorName={authorName[0]?.name}/>
        )
    });

    return (
        <div className='book-list mt-3'>
            <h2 className='ml-2 mt-2'>Книги:</h2>
            <ul className='app-list list-group mt-2'>
                <AppFilter authorList={authorList} onFilterSelect={onFilterSelect} filter={filter}/>
                <li className='book-list-title d-flex justify-content-between'>
                    <span>Название:</span>
                    <span>Автор:</span>
                    <div className='d-flex justify-content-center align-items-center'>
                        <i className='fas fa-star'></i>
                    </div>
                </li>
                {itemsBooks}
                {itemsBooks.length === 0 ? <div className='book-list-title'>Книг не найдено</div> : null}
            </ul>
        </div>
    )
}

export default BookList;