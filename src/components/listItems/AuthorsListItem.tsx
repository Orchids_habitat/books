import React, { FC } from 'react';

import './listItems.css';

interface AuthorsListItemProps {
    name: string
}

const AuthorsListItem: FC<AuthorsListItemProps> = ({name}) => {
        
    return (
        <li className='list-group-item list-group-item-authors d-flex justify-content-between'>
            <span className='list-group-item-author'>{name}</span>
        </li>
    )
}

export default AuthorsListItem;