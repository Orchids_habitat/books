import React, { useState, useEffect, FC } from 'react';
import { addAuthor } from '../../api/BookService';
import Input from '../../utils/Input';

import './addForm.css'

interface AuthorAddFormProps {
    onAddAuthor: () => void
}

const AuthorAddForm: FC<AuthorAddFormProps> = ({onAddAuthor}) => {
    const [name, setName] = useState<string>('')
    const [bio, setBio] = useState<string>('')
    const [birth_date, setBirth_date] = useState<string>('')
    const [formValid, setFormValid] = useState<boolean>(false)

    const data = {
        name, 
        bio, 
        birth_date, 
        setName, 
        setBio, 
        setBirth_date, 
        onAddAuthor
    }

    useEffect(() => {
        if (!name || !bio || !birth_date) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [name, bio, birth_date])

    return (
        <div className='app-add-form mt-3 p-3'>
            <div className='add-form'>
                <h3>Добавьте автора</h3>
                <Input value={name} setValue={setName} type='text' placeholder='Введите имя'/>
                <Input value={bio} setValue={setBio} type='text' placeholder='Введите биографию'/>
                <Input value={birth_date} setValue={setBirth_date} type='date' placeholder='Введите дату рождения'/>
                <button disabled={!formValid} 
                        className='btn btn-outline-primary mt-2 mr-2' 
                        onClick={() => addAuthor(data)}>
                        {!formValid ? 'Заполните все поля' : 'Добавить'}
                </button>
            </div>
        </div>
    );
};

export default AuthorAddForm;