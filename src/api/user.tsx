import axios from 'axios'
import userStore from '../store/userStore';
import { IUserReg, IUserLog } from '../types/types';

const _api = 'https://mobile.fakebook.press/api/';

//Регистрация
export const registration = async ({name, email, password, password_confirmation, setName, setEmail, setPassword, setPassword_confirmation}: IUserReg) => {
    try {
        await axios.post(`${_api}register`, {
            name: name,
            email: email,
            password: password,
            password_confirmation: password_confirmation
        })
        setName('');
        setEmail('')
        setPassword('')
        setPassword_confirmation('')
        alert('Успешная регистрация')
    } catch (e) {
        alert('Ошибка регистрации')
    }
}

//Авторизация с установкой пользователя и токена
export const login = async ({email, password}: IUserLog) => {
    try {
        const response = await axios.post(`${_api}login`, {
            email: email,
            password: password,
        })
        localStorage.setItem('token', response.data.data.token)
        const res = await axios.get(`${_api}me`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        userStore.setUser(res.data.data);
    } catch (e) {
        alert('Ошибка авторизации')
    }
}

//Проверка авторизации при перезагрузке
export const auth = async (isLoaded: () => void) => {
    try {
        if (localStorage.getItem('token')) {
            const res = await axios.get(`${_api}me`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            userStore.setUser(res.data.data);
        }
        isLoaded()
    } catch (e) {
        localStorage.removeItem('token')
    }
}