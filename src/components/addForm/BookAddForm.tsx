import React, { useState, useEffect, FC } from 'react';
import { addBook } from '../../api/BookService';
import { IAuthor } from '../../types/types';
import Input from '../../utils/Input';
import Select from '../../utils/Select'

import './addForm.css'

interface BookAddFormProps {
    authorList: IAuthor[], 
    onAddBook: () => void
}

const BookAddForm: FC<BookAddFormProps> = ({authorList, onAddBook}) => {
    const [name, setName] = useState<string>('')
    const [author_id, setAuthor_id] = useState<string>('')
    const [publication_date, setPublication_date] = useState<string>('')
    const [desc, setDesc] = useState<string>('')
    const [formValid, setFormValid] = useState<boolean>(false)

    const data = {
        name, 
        author_id, 
        desc, 
        publication_date, 
        setName, 
        setAuthor_id, 
        setPublication_date, 
        setDesc, 
        onAddBook
    }

    useEffect(() => {
        if (!name || !author_id || !publication_date ||!desc) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [name, author_id, publication_date, desc])

    return (
        <div className='app-add-form mt-3 p-3'>
            <div className='add-form'>
            <h3>Добавьте книгу</h3>
                <Input value={name} setValue={setName} type='text' placeholder='Введите название'/>
                <Input value={publication_date} setValue={setPublication_date} type='date' placeholder='Введите дату публикации'/>
                <Input value={desc} setValue={setDesc} type='text' placeholder='Введите описание'/>
                <Select value={author_id} setValue={setAuthor_id} authorList={authorList}/>
                <button disabled={!formValid} 
                        className='btn btn-outline-primary mt-2 mr-2' 
                        onClick={() => addBook(data)}>
                        {!formValid ? 'Заполните все поля' : 'Добавить'}
                </button>
            </div>
        </div>
    );
};

export default BookAddForm;