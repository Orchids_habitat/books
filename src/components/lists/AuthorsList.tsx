import React, { FC } from 'react';
import AuthorsListItem from '../listItems/AuthorsListItem';
import { IAuthor } from '../../types/types';
import './lists.css';

interface AuthorListProps {
    authorList: IAuthor[]
}

const AuthorList: FC<AuthorListProps> = ({authorList}) => {


    const itemsAuthors =  authorList.map(item => {
        const {id, ...itemProps} = item;
        return (
            <AuthorsListItem key = {item.id} {...itemProps}/>
        )
    });

    return (
        <div className='book-list mt-3'>
            <h2 className='ml-2 mt-2'>Авторы:</h2>
            <ul className='app-list list-group mt-2'>
                {itemsAuthors}
                {itemsAuthors.length === 0 ? <div className='book-list-title'>Авторов не найдено</div> : null}
            </ul>
        </div>
    )
}

export default AuthorList;