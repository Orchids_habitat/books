import React, { FC } from 'react';
import { IAuthor } from '../../types/types';

import './appFilter.css';

interface AppFilterProps {
    authorList: IAuthor[], 
    filter: string, 
    onFilterSelect: (filter: string) => void
}

const AppFilter: FC<AppFilterProps> = ({authorList, filter, onFilterSelect}) => {

    const authors = authorList.map((item) => {
        const active = filter === item.id;
        const clazz = active ? 'btn-light' : 'btn-outline-light';
        return (
            <button key={item.id} 
                    value={item.id}
                    name={item.name}
                    type='button'
                    className={`btn ${clazz}`}
                    onClick={() => onFilterSelect(item.id)}>
                {item.name}
            </button>
        )
    });
    
    return (
        <div className='btn-group'>
                <button type='button' 
                        className={`btn ${filter === 'all' ? 'btn-light' : 'btn-outline-light'}`} 
                        name='all' 
                        onClick={() => onFilterSelect('all')}>Все авторы</button>
            {authors}
        </div>
    )
}

export default AppFilter;