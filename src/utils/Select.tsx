import React, { FC } from 'react';
import { IAuthor } from '../types/types';

interface SelectProps {
    value: string;
    authorList: IAuthor[];
    setValue: (value: string) => void;
}

const Select: FC<SelectProps> = ({authorList, setValue, value}) => {

    const options = authorList.map((item) => {
        return (
            <option key = {item.id} value={item.id}>{item.name}</option>
        )
    });

    return (
        <div className='input-group mb-3 mt-3'>
            <div className='input-group-prepend'>
                <label className='input-group-text' htmlFor='inputGroupSelect01'>Автор</label>
            </div>
            <select className='custom-select' id='inputGroupSelect01' 
            onChange={(event)=> setValue(event.target.value)}
            value={value}>
                <option  value=''>Выберите автора</option>
                {options}
            </select>
        </div>
    );
};

export default Select;