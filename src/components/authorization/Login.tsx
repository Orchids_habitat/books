import React, { useState, useEffect } from 'react';
import Input from '../../utils/Input';
import { login } from '../../api/user';

import './authorization.css'

const Login = () => {
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [formValid, setFormValid] = useState<boolean>(false)

    const data = {
        email, 
        password,
        setEmail, 
        setPassword
    }

    useEffect(() => {
        if (!email || !password) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [email, password])

    return (
        <div className='authorization'>
            <div className='authorization__header'>Авторизация</div>
            <Input value={email} setValue={setEmail} type='text' placeholder='Введите email'/>
            <Input value={password} setValue={setPassword} type='password' placeholder='Введите пароль'/>
            <button disabled={!formValid} className='authorization__btn' onClick={() => login(data)}>Войти</button>
        </div>
    );
};

export default Login;