import React, { FC } from 'react';

import './input.css'

interface InputProps {
    value: string;
    type: string;
    placeholder: string;
    setValue: (value: string) => void;
}

const Input: FC<InputProps> = ({setValue, value, type, placeholder}) => {
    
    return (
        <input onChange={event => setValue(event.target.value)}
               value={value}
               type={type}
               placeholder={placeholder}/>
    );
};

export default Input;