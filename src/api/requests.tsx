//разобраться
const axios = require('axios').default

const _api = 'https://mobile.fakebook.press/api/';

const headers = () => {
    const token = localStorage.getItem('token')
    
    return (token ? {Authorization: `Bearer ${token}`} : {})
}

export async function getRequest(url: string, data?: Object) {
    return axios({
      url,
      method: 'get',
      _api,
      headers: headers(),
      params: data,
    })
}

export async function postRequest(url: string, data: Object) {
    return axios({ 
        url, 
        method: 'post', 
        _api, 
        headers: headers(), 
        data 
    })
}

export async function deleteRequest(url: string, data?: Object) {
    return axios({ 
        url, 
        method: 'delete', 
        _api, 
        headers: headers(), 
        params: data })
}  