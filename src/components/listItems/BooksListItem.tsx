import React, { useState } from 'react';
import { favBook } from '../../api/BookService';
import { IBook } from '../../types/types';

import './listItems.css';

const BooksListItem = ({name, id, authorName}: IBook) => {

    const [fav, setFav] = useState<boolean>(false)

    const onFav = () => {
        setFav(!fav)
        if (!fav) {
            const data = {
                id: id,
                action: 'add-to',
                alertMessage: 'Добавлено в избранное'
            }
            favBook(data)
        } else {
            const data = {
                id: id,
                action: 'remove-from',
                alertMessage: 'Удалено из избранного'
            }
            favBook(data)
        }
    }

    let classNames = 'list-group-item list-group-item-books d-flex justify-content-between';

    if (fav) {
        classNames += ' like';
    }

    return (
        <li className={classNames} onClick={onFav}>
            <span className='list-group-item-book'>{name}</span>
            <span className='list-group-item-author'>{authorName}</span>
            <div className='d-flex justify-content-center align-items-center'>
                <i className='fas fa-star'></i>
            </div>
        </li>
    )
}

export default BooksListItem;