export interface IUser {
    name: string,
    id: string,
    email: string
}

export interface IBook {
    name: string,
    authorName: string,
    id: string
}

export interface IFetchBook {
    name: string,
    author_id: string,
    id: string,
}

export interface IAuthor {
    name: string,
    id: string
}

export interface IUserReg {
    name: string, 
    email: string, 
    password: string, 
    password_confirmation: string, 
    setName: (str: string) => void, 
    setEmail: (str: string) => void,  
    setPassword: (str: string) => void, 
    setPassword_confirmation: (str: string) => void
}

export interface IUserLog {
    email: string, 
    password: string
}

export interface IAddAuthor {
    name: string, 
    bio: string, 
    birth_date: string, 
    setName: (str: string) => void, 
    setBio: (str: string) => void, 
    setBirth_date: (str: string) => void, 
    onAddAuthor: () => void
}

export interface IAddBook {
    name: string, 
    author_id: string, 
    desc: string, 
    publication_date: string, 
    setName: (str: string) => void, 
    setAuthor_id: (str: string) => void, 
    setPublication_date: (str: string) => void, 
    setDesc: (str: string) => void, 
    onAddBook: () => void
}

export interface IFavBook {
    id: string,
    action: string,
    alertMessage: string
}